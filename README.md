# INTRODUCTION
-----

To improve release quality and for a better collaboration between every developer, a common git usage and some basic rules need to be apply by everyone for productivity and to avoid problems. That's what we all want, right? 😄 

In this workflow, there is two main branches:
* ___master:___ Contains only release commit with a ___tag___
* ___develop:___ Contains code for future releases

# GENERAL RULES
-----

___master contains only release commit___ (Yeah, I know, I wrote it just above, but it's really important 😁 )

___Always create a branch___ if you want to do modifications. Try to not push directly on develop unless there is an urgent need

___NEVER push force on master / develop___

___name your branch according what it does___ (feature/XXXXX, bug/XXXXX, refactor/XXXXX, hotfix/XXXXX, release/XXXXX)

___always rebase___ your branch on the branch which is its base before merg

___Try to not create a branch not based on develop.___ Finish your previous one, merge it on develop, then create a new one.

# BRANCHES

## Development branches:

### feature
Develop a new feature for a future release.
* Based on: develop
* Merge on: develop (no-ff)


### bug
Fix a bug for a future release.
* Based on: develop
* Merge on: develop (no-ff)

### refactor
Improve code quality for a future release
* Based on: develop
* Merge on: develop (no-ff)

## Release branches:

### release
When develop contains all the bug fixes and the features needed for the next release, we create a new release branch. This branch will progress until every bug is fixed. When it's done, ___we merge this branch WITHOUT rebase to develop and master___
* Based on: develop
* Merge on: develop / master

### hotfix
When a critical bug is spotted on production and we need to make a release to fix it, we use a hotfix branch. This branch work exactly the same as the release, except that it's based on master and not develop.
* Based on: master
* Merge on: develop / master

# HOW TO MERGE MY DEVELOPMENT BRANCH ON DEVELOP
-----

-Update your develop:

```
git checkout develop; git pull origin develop
```

-Rebase your branch => While being on your branch: 

```
git checkout name_of_your_branch; git rebase origin/develop
```

-Push your rebased branch 

```
 git push origin name_of_your_branch -f
```

-Go on master and merge your branch (without fast forward) 

```
git checkout develop
```
```
git merge name_of_your_branch --no-ff
```

-Push

```
 git push origin develop
```

Then you're done 👍 